<?php
include ('../vendor/autoload.php');
//echo '<pre>';
//var_dump($_SERVER);
//echo '</pre>';
/*
https://tempelers-j-tjeremy.c9users.io/mmt-php-api/public/curiosity/readall
*/
$request = $_SERVER['REDIRECT_URL'];
// echo "<br />Request: $request";
switch ($request) {
    
    // proberen met get
    case '/mmt-php-api/public/curiositycomment/ownCreate' :
        $curiosityId = $_GET['CuriosityId'];
        $userName = $_GET['UserName'];
        $comment = $_GET['Comment'];
        $newComment = array(
                "CuriosityId" => $curiosityId,
                "Comment" =>$comment,
                "UserName" => $userName,
            );
        \ModernWays\Dal::$configLocation = __DIR__ . '/../data/config.ini';    
        if (\ModernWays\Dal::create('CuriosityComment', $newComment, 'Comment')) {
            echo 'Create is gelukt!<br/>';
        } else {
            echo 'Oeps er is iets fout gelopen!</br>';
        }
        echo \ModernWays\Dal::getMessage();        
        break;
    // einde proberen
    
    case '/mmt-php-api/public/curiosity/readall' :
        \ModernWays\Dal::$configLocation = __DIR__ . '/../data/config.ini';    
        $list = \ModernWays\Dal::readAll('Curiosity');
        //$json = '{' . json_encode($list) . '}';
        echo json_encode($list);
        break;
    case '/mmt-php-api/public/curiositycomment/create' :
            $newComment = [
                'CuriosityId' => $_GET('CuriosityId'),
                'Comment' => $_GET('Comment'),
                'UserName' => $_GET('UserName'),
            ];
        \ModernWays\Dal::$configLocation = __DIR__ . '/../data/config.ini';    
        if (\ModernWays\Dal::create('CuriosityComment', $newComment, 'Comment')) {
            echo 'Create is gelukt!<br/>';
        } else {
            echo 'Oeps er is iets fout gelopen!</br>';
        }
        echo \ModernWays\Dal::getMessage();        
        break;
     case '/mmt-php-api/public/curiositycomment/createcheck' :
        if (isset($_POST['UserName']) && isset($_POST['Password'])) {
            \ModernWays\Dal::$configLocation = __DIR__ . '/../data/config.ini';    
           // find user and verify password
            $user = \ModernWays\Dal::readOne('User', $_POST['UserName'], 'Name', array('Name', 'HashedPassword', 'RoleId', 'Salt'));
            if (isset($user)) {
                if (password_verify($_POST['Password'] . $user['Salt'], $user['HashedPassword'])) {
                    $newComment = array('UserName' => $user['Name'], 'Comment' => $_POST['Comment'], 'CuriosityId' => $_POST['CuriosityId']);
                    if (\ModernWays\Dal::create('CuriosityComment', $newComment, 'Comment')) {
                        echo 'Create is gelukt!<br/>';
                    } else {
                        echo 'Oeps er is iets fout gelopen!</br>';
                    }
                    echo \ModernWays\Dal::getMessage(); 
                } else {
                    echo 'Ongeldig paswoord!';
                }
            } else {
                echo 'Ongeldige gebruikernaam!';
            }
        } else {
            echo 'Je moet een gebruikernaam en paswoord opgeven!';
        }
            
        break;
   case '/mmt-php-api/public/curiositycomment/readallwhere' :
        if (isset($_GET['CuriosityId'])) {
        \ModernWays\Dal::$configLocation = __DIR__ . '/../data/config.ini';    
            $list = \ModernWays\Dal::readAllWhere('CuriosityComment', $_GET['CuriosityId'], 'CuriosityId', 'Id');
            // $json = '{' . json_encode($list) . '}';
            echo json_encode($list);
        } else {
            echo "$request is invalid and cannot be processed.";
        }
        break;
    default: 
        echo "$request not found.";
        break;
}