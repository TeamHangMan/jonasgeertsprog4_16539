<?php
/**
 * Created by PhpStorm.
 * User: jonas
 * Date: 19/01/2019
 * Time: 11:35
 */

include ('config.php');

try {
    // backslashes gebruiken we om namespaces aan te geven.
    // Eén backslash = de klasse staat in de rootnamespace.
    $connection = new \PDO($host, $username, $password, $options);
    $sql = file_get_contents("data/sqlscripts/init.sql");
    $connection->exec($sql);

    echo "Database and table Users created successfully.";
} catch(\PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}