<?php
/**
 * Created by PhpStorm.
 * User: jonas
 * Date: 19/01/2019
 * Time: 11:35
 */

$username = 'root';
$databaseName = 'TaniaRascia';
// hier geven we de databasenaam mee
$host = "mysql:host=localhost:3306;dbname=$databaseName";
$password = '';
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);