<?php
/**
 * Created by PhpStorm.
 * User: jonas
 * Date: 19/01/2019
 * Time: 11:32
 */

function escape($html) {
    return htmlspecialchars($html, ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8");
}